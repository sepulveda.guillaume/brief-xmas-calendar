import React, { useState } from 'react';

export function Cards(props) {
	//console.log(props);
	
	return (
		<div className={`Cards ${props.state}`} key={`${props.id}`} onClick={() => props.switchCard(props.item, true)}>
			<div>
				<div class="verso">
					<p class="number">{`${props.id}`}</p>
				</div>
				<div class="recto">
					<p class="expression">{`${props.texte}`}</p>
					<p class="auteur"> -- {`${props.auteur}`}</p>
				</div>				
			</div>
		</div>
	);
}
